# Mozilla Add-ons Dark Theme

A dark theme for the website [Mozilla Add-ons](https://addons.mozilla.org) (aka AMO).

This style has been written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## Install with Stylus

If you have Stylus installed, click on the banner below and a new window of Stylus should open, asking you to confirm to install the style.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/amo-dark/raw/master/mozilla-addons-dark.user.css)

## Prerequisites

The following can also be read in Stylus when you navigate to AMO and bring up the panel of Stylus; I'm including a paraphrased version here for reference:

- Firefox 60+ (it might work with a web browser based on it, too, although I haven't tested it on anything else yet)
- In `about:config`…
	1. Add a new boolean (with the right click context menu) with the name `privacy.resistFingerprinting.block_mozAddonManager` and set its value to `true`.
	   * This will disable all “special” AMO privileges such as determining which add-ons are already installed or uninstalling add-ons from the website.
	2. Remove the substring `addons.mozilla.org,` from `extensions.webextensions.restrictedDomains`.
	   * This will allow all currently installed WebExtensions to manipulate the contents of AMO websites and APIs.

## Screenshots

![A screenshot showing the main page](screenshot01.png)

![A screenshot showing the page of an extension](screenshot02.png)

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
